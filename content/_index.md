---
title: "Home"
date: 2018-03-24T19:55:15-03:00
draft: false
---

LKCAMP is a Linux Kernel study group open for everyone to participate.

Please, see [About](/about) and
[Documentation](https://lkcamp.gitlab.io/lkcamp_docs/) pages for more
information and how to get started.


# Round 4 - Campinas/Brazil (On Going - 1o Sem 2020)

NOTE: Due to COVID-19, we are holding remote meetings since April 14. Please get in touch for more information.

### Attending the meetings
- **When:** Every Tuesday at 19h30 (Sao Paulo time) starting on 10 Mar 2020.
- **Where:** Remote (links are published in the mailing list or IRC/Telegram channel just before)
- **Register** to attend the meetings through the
[registration form](https://lkcamp.persona.ninja/limesurvey/index.php/135389)

NOTE: Meetings are held in Portuguese, but every body is welcome to chat in English through our mailing list or IRC/Telegram channel.

### Remote participation
- **Live stream:** [YouTube IC
  channel](https://www.youtube.com/channel/UCraCE6iWUcFCJSp-vmO1D3A/videos), [YouTube LKCAMP channel](https://www.youtube.com/channel/UC2skRId4WWg9F0vc6GAKRIA)
- **Mailing list:** lkcamp@lists.libreplanetbr.org - [subscribe
  here](https://lists.libreplanetbr.org/mailman/listinfo/lkcamp)
- **IRC Channel:** #lkcamp @ Freenode
- **Telegram (IRC bridge):** [https://t.me/lkcamp](https://t.me/lkcamp)

### Planned meetings - 2020

   * 10 Mar (tue):                    Workshop: sending your first contribution [video](https://www.youtube.com/watch?v=y6HxuJG1hQw)
   * 14 Apr (tue):                    Workshop: sending your first contribution (cont) [video](https://www.youtube.com/watch?v=_9tBWdj3m2o)
   * 23 Apr (**thu**):                    Git: crash course [video](https://youtu.be/mUjV22KXSQs)
   * 28 Apr (tue):                    Device Drivers
   *  5 May (tue):                    Syscalls
   * Weekly meetings:                 Advanced topics
   * TBD (sat/sun):                   LKCAMPING video4linux
   * Happy Hour (TBD)

![lkcamp-round4-flyer](imgs/lkcamp-divulgacao-round4.png)


# Previous rounds

## Round 3 - Campinas/Brazil

### Mettings

- [M1: Welcome to LKCAMP!](https://lkcamp.gitlab.io/lkcamp_docs/unicamp_group/boot/)
    - When: 26/Mar/2019
    - Presenter: Helen Koike
    - [slides](/slides/round3/lkcamp-M1.pdf)
    - [video](https://www.youtube.com/watch?v=BmBzNDzLi0w)

- [M2: First Contribution](https://lkcamp.gitlab.io/lkcamp_docs/unicamp_group/first_contrib/)
    - When: 02/Apr/2019
    - Presenters: Gabriel Mandaji / Danilo Rocha
    - [slides](/slides/round3/lkcamp-M2.pdf)
    - [video](https://www.youtube.com/watch?v=VCwFkbxoY7I)

- [M3: Device Drivers](https://lkcamp.gitlab.io/lkcamp_docs/unicamp_group/dev_drivers/)
    - When: 09/Apr/2019
    - Presenters: Laís Pessine / Helen Koike
    - [slides](/slides/round3/lkcamp-M3.pdf)
    - [video](https://www.youtube.com/watch?v=-ge-X0aUhJo)

- M4: How the system boots
    - When: 16/Apr/2019
    - Presenter: Lucas Magalhães
    - [slides](/slides/round3/lkcamp-M4.pdf)
    - [video](https://www.youtube.com/watch?v=XQE_pizWV8c)

- [M5: System calls](https://lkcamp.gitlab.io/lkcamp_docs/unicamp_group/systemcalls/)
    - When: 23/Apr/2019
    - Presenter: Nícolas F. R. A. Prado
    - [slides](/slides/round3/lkcamp-M5.pdf)
    - [video](https://www.youtube.com/watch?v=NCZiXmjYZHg)


Check our videos from previous rounds at [the archived](/archived/) page

## Round 1 - Blumenau-SC/Brazil

### Mettings

- M1: Introdução ao Kernel do Linux
    - When: 21/May/2019
    - Presenters: David Emmerich Jourdain / Marcos Paulo de Souza
    - [slides](/slides/round1-bnu/lkcamp-M1.pdf)
    - [video](https://www.youtube.com/watch?v=Sjx2QO_t0EM)

- M2: Hello World! Creating your first device driver in Linux Kernel
    - When: 25/Jun/2019
    - Presenters: Marcos Paulo de Souza

## Please edit our page

We are all volunteers, any help is welcomed.
The code of this page is available at [https://gitlab.com/lkcamp](https://gitlab.com/lkcamp)
If you want to help us to improve this page, please send us Pull Requests.
