---
title: "Round2"
date: 2019-01-19T19:55:15-03:00
draft: false
---

## Round 2

### Attending the meetings
- **When:** Every Tuesday at 19h (Sao Paulo time) starting on 16 Oct 2018
- **Where:** [room 352 IC3.5 @ Unicamp (Campinas/SP Brazil)](https://www.openstreetmap.org/way/95011172)

### Hacking meetings
- **When:** Every Wednesday at 19h (Sao Paulo time) starting on 17 Oct 2018
- **Where:** [room 352 IC3.5 @ Unicamp (Campinas/SP Brazil)](https://www.openstreetmap.org/way/95011172)
- **Required:** Bring your own computer with sudo powers

### Meetings (Phase 1)

- [M1: Group presentation](https://lkcamp.gitlab.io/lkcamp_docs/phase_1/M01/)
    - When: 16/Out/2018
    - Presenter: Guilherme Gallo / Helen Koike
    - [video](https://www.youtube.com/watch?v=QbFDGzv1gUU),
    - [slides](/slides/round2/lkcamp-M1.odp),
    - English subtitle: no

- [M2: Device Drivers](https://lkcamp.gitlab.io/lkcamp_docs/phase_1/M02/)
    - When: 23/Out/2018
    - Presenter: Lucas Magalhães
    - [video](https://www.youtube.com/watch?v=cxVJLdYmVZ4),
    - [slides](https://gitlab.com/lucmaga/talks/blob/58e504141106dbb20deaa9a37c2599de5220a837/M2/presentation.pdf),
    - English subtitle: no

- M2.5: Git Basics
    - When: 30/Out/2018
    - Presenter: Lucas Magalhães / Helen Koike
    - [video](https://www.youtube.com/watch?v=GOHm0haXfUo),
    - [slides](https://gitlab.com/lucmaga/talks/blob/58e504141106dbb20deaa9a37c2599de5220a837/git/presentation.pdf),
    - English subtitle: no

- [M3: The Linux Kernel Development Cycle](https://lkcamp.gitlab.io/lkcamp_docs/phase_1/M03/)
    - When: 06/Nov/2018
    - Presenter: Guilherme Gallo
    - [slides](/slides/round2/lkcamp-M3.odp),
    - English subtitle: no
