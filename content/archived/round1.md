---
title: "Round1"
date: 2018-03-24T19:55:15-03:00
draft: false
---

Lkcamp is a Linux Kernel study group open for everyone to participate.

Please, see [About](/about) and [Documentation]
(https://lkcamp.gitlab.io/lkcamp_docs/) pages for more information and how to
get started.

# Please edit our page

We are all volunteers, any help is welcomed.
The code of this page is available at [https://gitlab.com/lkcamp](https://gitlab.com/lkcamp)
If you want to help us to improve this page, please send us Pull Requests.

# Practical information

## Attending the meetings
- **When:** Every Tuesday at 19h (Sao Paulo time)
- **Where:** room 352 IC3.5 @ Unicamp (Campinas/SP Brazil)
- **Required:** Bring your own computer with sudo powers

## Remote participation
- **Live stream:** [YouTube IC channel]
(https://www.youtube.com/channel/UCraCE6iWUcFCJSp-vmO1D3A/videos) 
- **Mailing list:** [lkcamp@lists.libreplanetbr.org]
(https://lists.libreplanetbr.org/mailman/listinfo/lkcamp)
- **IRC Channel:** #lkcamp @ Freenode

# Meetings (Phase 1)

- [M1: Group presentation](https://lkcamp.gitlab.io/lkcamp_docs/phase_1/M01/)
    - When: 20/Mar/2018
    - Presenter: Helen Koike
    - [video](https://youtu.be/YKARS9Sz-Cc),
    - [slides](/slides/round1/lkcamp-M1.odp),
    - English subtitle: yes

- [M2: Miscellaneous information about the kernel]
(https://lkcamp.gitlab.io/lkcamp_docs/phase_1/M02/)
    - When: 27/Mar/2018
    - Presenter: Helen Koike
    - [video](https://youtu.be/T4QNorHHkbk),
    - [slides](/slides/round1/lkcamp-M2.odp),
    - English subtitle: yes

- [M3: The Linux Kernel Development Cycle]
(https://lkcamp.gitlab.io/lkcamp_docs/phase_1/M03/)
    - When: 03/Apr/2018
    - Presenter: Gabriel Krisman
    - [video](https://youtu.be/SgPFFDf3RH0),
    - [slides](/slides/round1/lkcamp-M3.odp),
    - English subtitle: WIP

- [M4: Linux Kernel Modules]
(https://lkcamp.gitlab.io/lkcamp_docs/phase_1/M04/)
    - When: 10/Apr/2018
    - Presenter: Helen Koike
    - [video](https://youtu.be/PxtPm6jK32k),
    - [slides](/slides/round1/lkcamp-M4.odp),
    - English subtitle: not yet

- [M5: The boot process in the GNU/Linux system & Linux Debugging]
(https://lkcamp.gitlab.io/lkcamp_docs/phase_1/M05/)
    - When: 19/Apr/2018
    - Presenter: Gabriel Krisman
    - [video](https://youtu.be/0zVIAIiLaK0),
    - [slides - The boot process in the GNU/Linux system](/slides/round1/lkcamp-M5-boot.odp),
    - [slides - Linux Debugging](/slides/round1/lkcamp-M5-debug.odp),
    - English subtitle: not yet

- [M6: Kernel and Userspace frontier, V4L subsystem]
(https://lkcamp.gitlab.io/lkcamp_docs/phase_1/M06/)
    - When: 24/Apr/2018
    - Presenter: Helen Koike
    - [video](https://youtu.be/hurdfwUL-ok),
    - [slides](/slides/round1/lkcamp_M6.odp),

- M-Hacking
    - When: 01/May/2018 (Holiday)
    - Note: Holiday, we can use the room but we won't have live stream (as we
      can't get the equipment from the University). So the ideia is to hack the
      kernel, ramp up people and maybe give a talk about basic things.

- [M7: An Overview of the Linux network stack]
(https://lkcamp.gitlab.io/lkcamp_docs/phase_1/M07/)
    - When: 15/May/2018
    - Presenter: Thadeu Cascardo
    - [video](/videos/network.webm),
    - [slides](/slides/round1/lkcamp-M7.odp),

- [M8: The Block IO subsystem]
(https://lkcamp.gitlab.io/lkcamp_docs/phase_1/M08/)
    - When: 22/May/2018
    - Presenter: Gabriel Krisman
    - [video (English)](https://people.collabora.com/~krisman/blocklayer.webm),

- [M9: The DRM/Graphics subsystem]
(https://lkcamp.gitlab.io/lkcamp_docs/phase_1/M09/)
    - When: TBD
    - Presenter: Gabriel Krisman

- [M10: Overview of the suggested projects for phase 2]
(https://lkcamp.gitlab.io/lkcamp_docs/phase_1/M10/)
    - When: 05/Jun/2018
    - Presenter: Helen Koike
