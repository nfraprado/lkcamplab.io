---
title: "About"
date: 2018-03-24T19:55:15-03:00
draft: false
---

Lkcamp is a Linux Kernel study group open for everyone to participate.

The group was created by volunteers in the University of Campinas (UNICAMP)
Brazil for people who wants to become a Linux Kernel developer.

Many people either don't know where to start, or start by sending a simple
patch to the kernel (e.g. fixing a typo or a coding style) and then don't know
what to do next.

The idea of the LKCamp is to provide mentorship and support as a group until
the participant is engaged in a more complex activity in the Linux Kernel
community.

To attend this goal we divided the group in 3 main phases:

- Phase 1) Learning the basis of the Linux Kernel development
- Phase 2) Working on a specific project
- Phase 3) Spreading the knowledge (pro level)

Please see the [Documentation](https://lkcamp.gitlab.io/lkcamp_docs/) for more
details on each phase.

# How to get started

You can start by reading the [Documentation]
(https://lkcamp.gitlab.io/lkcamp_docs/) which describes step by step all the
activities.

# Meetings

We are having weekly meetings. Each meeting is composed by a talk followed by a
proposed activity for that meeting. The idea is to start hacking the Linux
Kernel since the first meeting M1.

You can attend the meetings in person or remotely.

NOTE: even if you didn't attend the meetings since day 1, you can still
watch the recorded videos and do the proposed activities, you can do the
activities in your own pace and we can help you thought the mailing list and
IRC channel (see Documentation).

See the [Home page](/) to check when and how to attend the meetings.

# FAQ

## Which are the requirements to participate?
You need a machine running Linux with sudo powers to be able to install a
custom kernel.

It would also help you to have:

- Notions of C programming language
- Notions of GNU/Linux command line
- Notions of data structures
- Notions of operating systems

Don't be afraid if you don't have the knowledges above, you can learn in ad-hoc
manner, we will be there to help with any questions, there are no stupid
questions.

## What is IRC? How can I connect to the chat room?
Read the [Documentation M1]
(https://lkcamp.gitlab.io/lkcamp_docs/phase_1/M01/#irc-channels)

## I don't speak Portuguese, can I participate?
Yes, but it won't be that easy, we are making an effort to add English
subtitles to the videos, but we are volunteers and sometimes we don't have time
to do it.
But you should be able to follow the group only using the [Documentation]
(https://lkcamp.gitlab.io/lkcamp_docs/), the [lkcamp@lists.libreplanetbr.org]
(https://lists.libreplanetbr.org/mailman/listinfo/lkcamp)
mailing list and the IRC channel #lkcamp @ Freenode.

NOTE: communication in the mailing list is usually in Portuguese, but feel free
to post in English. Communication on IRC is usually in English.

## My English is not good, can I still participate?
Sure, the Linux Kernel community is composed from people all over the world and
we understand that English may not be your first language.
If you make your self understandable, it is good enough, no one will judge you
by your English.

## What is M1, M2, M3... ?
M stands for Meeting (or Module). M1 is the first meeting, M2 the second
meeting and so on.
In the [Documentation](https://lkcamp.gitlab.io/lkcamp_docs/) you will find a
description of each meeting and the proposed activity.

## I didn't attend some of the meetings, can I still participate?
Of course, we understand that people have different backgrounds and can do the
activities in the own pace.
You can also participate asynchronously, use the mailing list and IRC channel
to make questions, we are there to help.

## When are you going to restart the meeting from M1?
We will probably do a second edition in August 2018, but nothing is confirmed.

## How the remote participation works?
You first watch the talk for that specific meeting, then read the
[Documentation](https://lkcamp.gitlab.io/lkcamp_docs/) and try to do the
proposed activity for that meeting, interacting with the group through IRC and
the mailing list.
